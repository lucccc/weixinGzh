package Service;

import com.baidu.aip.ocr.AipOcr;
import com.thoughtworks.xstream.XStream;
import entity.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.jetbrains.annotations.Contract;
import util.TulingTool;
import util.Util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static util.Main.API_KEY;
import static util.Util.beanToXml;


public class WxService {
    /*
     *
     * 以下配置信息最好是写在配置文件里
     * */
    //微信
    public static final String TOKEN = "123";
    //appID
    public static final String WX_APPID = "wx00a1d18cc3db80c2";
    //appsecret
    public static final String WX_APPSECRET = "58da4523570ca3e19d504bd8b97d8898";
    //ACCESS_TOKEN_URL
    public static final String WX_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";


    //图灵机器人
    public static final String TULING_API_URL = "http://openapi.tuling123.com/openapi/api/v2";
    //图灵机器人的apiKey
    public static final String TULING_APIKEY = "3e027125e8304b4882e122a4b188e61f";
    //图灵机器人的密钥
    public static final String TULING_PRIMARYKEY = "d19628b1e6c43a39";


    //百度AI
    public static final String BAIDU_APP_ID = "15242965";
    public static final String BAIDU_API_KEY = "tpCkWhEO3TdcBfWHtmknE5cA";
    public static final String BAIDU_SECRET_KEY = "M5Hli3u1NfURfUVg0HBsS4HwcvmBin5z";

    //用于存储token
    private static AccessToken at;

    /**
     * 获取token
     */
    public static void getToken() {
        String url = WX_ACCESS_TOKEN_URL.replace("APPID", WX_APPID).replace("APPSECRET", WX_APPSECRET);
        String tokenStr = null;
        try {
            tokenStr = Util.get(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // return tokenStr;
        JSONObject jsonObject = JSONObject.fromObject(tokenStr);
//
        String token = jsonObject.getString("access_token");
        String expireIn = jsonObject.getString("expires_in");
        //创建token对象,并存起来。
        at = new AccessToken(token, expireIn);
    }

    /**
     * 向处暴露的获取token的方法
     *
     * @return
     */
    public static String getAccessToken() {
        if (at == null || at.isExpired()) {
            getToken();
        }
        return at.getAccessToken();
    }

    /**
     * 验证签名
     *
     * @param timestamp
     * @param nonce
     * @param signature
     * @return
     */
    public static boolean check(String timestamp, String nonce, String signature) {
        //1）将token、timestamp、nonce三个参数进行字典序排序
        String[] strs = new String[]{TOKEN, timestamp, nonce};
        Arrays.sort(strs);
        //2）将三个参数字符串拼接成一个字符串进行sha1加密
        String str = strs[0] + strs[1] + strs[2];
        String mysig = sha1(str);
        //3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
        return mysig.equalsIgnoreCase(signature);
    }


    /**
     * 校验微信接入签名
     *
     * @return
     */
    public static boolean check(String signature, String timestamp, String nonce, String echostr) {
        //1）将token、timestamp、nonce三个参数进行字典序排序
        String[] strs = new String[]{TOKEN, timestamp, nonce};
        Arrays.sort(strs);
        //2）将三个参数字符串拼接成一个字符串进行sha1加密
        String str = strs[0] + strs[1] + strs[2];
        String mysig = sha1(str);
        //3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
        return mysig.equalsIgnoreCase(signature);
    }

    /**
     * sha1加密
     **/
    private static String sha1(String str) {
        try {
            //获取一个加密对象
            MessageDigest md;
            md = MessageDigest.getInstance("sha1");
            //加密
            byte[] digest = md.digest(str.getBytes());
            char[] chars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            StringBuilder sb = new StringBuilder();
            //处理加密结果
            for (byte b : digest) {
                sb.append(chars[(b >> 4) & 15]);
                sb.append(chars[b & 15]);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    /*
     * 获取图灵机器人聊天的回复内容
     * */
    public static String chat(String msg) throws IOException {
        //把输入的文本数据存储在请求实体类中
        String APIKEY = TULING_APIKEY;
        String question = URLEncoder.encode("你叫什么名字", "GBK");//这是上传给云机器人的问题
        System.out.println("msg: " + question);
        String getURL = "http://www.tuling123.com/openapi/api?key=" + APIKEY + "&info=" + msg;
        URL getUrl = new URL(getURL);
        HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
        connection.connect();
        //取得输入流，并使用Reader读取
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        //断开连接
        connection.disconnect();
        System.out.println("sb的值" + sb);
        return sb.toString();
    }

    public static String Newchat(String msg, String userid) {
        TulingTool tulingTool = new TulingTool();
        //封装消息的Json String
        String reqStr = tulingTool.getReqMes(msg, userid);
        System.out.println(reqStr);
        //返回的Json String
        String respStr = tulingTool.tulinPost(TULING_API_URL, reqStr);
        //获取图灵机器人回话String
        String result = tulingTool.getResultMes(respStr);
        System.out.println(result);
        return result;
    }

    /******
     * 解析xml数据包
     */
//    <xml>
//       <ToUserName>< ![CDATA[toUser] ]></ToUserName>
//       <FromUserName>< ![CDATA[fromUser] ]></FromUserName>
//       <CreateTime>1348831860</CreateTime>
//       <MsgType>< ![CDATA[text] ]></MsgType>
//       <Content>< ![CDATA[this is a test] ]></Content>
//       <MsgId>1234567890123456</MsgId>
//    </xml>
    public static Map<String, String> parseRequest(InputStream is) {
        Map<String, String> map = new HashMap<>();
        SAXReader reader = new SAXReader();
        try {
            //读取输入流，获取文档对象
            Document document = reader.read(is);
            //根据文档对象获取根节点
            Element root = document.getRootElement();
            //获取根节点的所有的子节点
            List<Element> elements = root.elements();
            for (Element e : elements) {
                map.put(e.getName(), e.getStringValue());
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 用于处理所有的事件和消息的回复
     *
     * @param requestMap ToUserName	开发者微信号
     *                   FromUserName	发送方帐号（一个OpenID）
     *                   CreateTime	消息创建时间 （整型）
     *                   MsgType	text
     *                   Content	文本消息内容
     *                   MsgId	消息id，64位整型
     * @return 返回的是xml数据包
     * by luchengsheng
     */
    public static String getRespose(Map<String, String> requestMap) {
        BaseMessage msg = null;
        String msgType = requestMap.get("MsgType");
        System.out.println("msgType:" + msgType);
        switch (msgType) {
            //处理文本消息
            case "text":
                msg = dealTextMessage(requestMap);
                break;
            case "image":
                msg = dealImage(requestMap);
                break;
            case "voice":

                break;
            case "video":

                break;
            case "shortvideo":

                break;
            case "location":

                break;
            case "link":

                break;
            case "event":
               msg = dealEvent(requestMap);
                break;
            default:
                break;
        }
        //把消息对象处理为xml数据包
        if (msg != null) {
            return beanToXml(msg);
        }
        return null;
    }






    /*
     *处理用户发过来的文本消息，系统自带表情，表情包属于图片消息
     */
    private static BaseMessage dealTextMessage(Map<String, String> requestMap) {
        //用户发来的内容
        String msg = requestMap.get("Content");
        String userid = requestMap.get("FromUserName");
        System.out.println("userID:" + userid);
        if (msg.equals("图文")) {
            List<Article> articles = new ArrayList<>();
            articles.add(new Article("漠河：零下30°C跳钢管舞", "12月21日报道，12月20日，第四届钢管舞极寒挑战赛在漠河北极村举行，来自中国16名竞技钢管舞爱好者和10名中国钢管舞国家队队员，共计26名选手身着背心短裤进行极寒挑战。来源：东方IC", "http://d.ifengimg.com/mw978_mh598/p3.ifengimg.com/2018_51/0a845f6f-04b8-407c-b198-756d4d3039a2_48E34ED051EEBFCD9694EDE56B149E4CE1C06D14_w1024_h682.jpg", "http://news.ifeng.com/a/20181221/60205388_0.shtml?_zbs_firefox#p=1"));
            NewsMessage nm = new NewsMessage(requestMap, articles);
            return nm;
        }
        if (msg.equals("登录")) {
            String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxb6777fffdf5b64a4&redirect_uri=http://www.6sdd.com/weixin/GetUserInfo&response_type=code&scope=snsapi_userinfo#wechat_redirect";
            TextMessage tm = new TextMessage(requestMap, "点击<a href=\"" + url + "\">这里</a>登录");
            return tm;
        }
        if (msg.equals("模板消息")){
            String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxb6777fffdf5b64a4&redirect_uri=http://www.6sdd.com/weixin/GetUserInfo&response_type=code&scope=snsapi_userinfo#wechat_redirect";
            TextMessage tm = new TextMessage(requestMap, "点击<a href=\"" + url + "\">这里</a>登录");
            return tm;
        } else {
            //调用聊天机器人接口生成回复消息返回聊天的内容
            String resp = Newchat(msg, userid);
            System.out.println(resp);
            TextMessage tm = new TextMessage(requestMap, resp);
            return tm;
        }
    }

    @Contract("_ -> new")
    private static BaseMessage dealImage(Map<String, String> requestMap) {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(BAIDU_APP_ID, BAIDU_API_KEY, BAIDU_SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 调用接口
        String path = requestMap.get("PicUrl");
        System.out.println(path);
        //进行网络图片的识别
        org.json.JSONObject res = client.generalUrl(path, new HashMap<String, String>());
        String json = res.toString();
        System.out.println("jsonPic:" + json);
        //转为jsonObject
        JSONObject jsonObject = JSONObject.fromObject(json);
        JSONArray jsonArray = jsonObject.getJSONArray("words_result");
        Iterator<JSONObject> it = jsonArray.iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            JSONObject next = it.next();
            sb.append(next.getString("words"));
        }
        return new TextMessage(requestMap, sb.toString());
    }


    private static BaseMessage dealEvent(Map<String, String> requestMap) {
        String event = requestMap.get("Event");
        switch (event) {
            case "CLICK":
                return dealClick(requestMap);
            case "VIEW":
                return dealView(requestMap);
            default:
                break;
        }
        return null;

    }

    private static BaseMessage dealView(Map<String, String> requestMap) {
        return null;
    }

    private static BaseMessage dealClick(Map<String, String> requestMap) {
        String key = requestMap.get("EventKey");
        switch (key) {
            //点击一菜单点
            case "31":
                //处理点击了第一个一级菜单
                return new TextMessage(requestMap, "你点了一点第一个一级菜单");
            case "32":
                //处理点击了第三个一级菜单的第二个子菜单
                return new TextMessage(requestMap, "哈哈哈哈");
            default:
                break;
        }
        return null;

    }


}
