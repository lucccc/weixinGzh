import Service.WxService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet(name = "WxServlet")
public class WxServlet extends HttpServlet {
    private static  final String STRING="";
    /*
    * 接收消息和事件的推送
    *
    * */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        System.out.println("post");
        //处理消息和时间推送
        Map<String, String> requestMap = WxService.parseRequest(request.getInputStream());
        //获取wxService处理的响应消息，返回给微信服务器一个xml,微信服务器解析xml就可以将我们xml里的信息转发给用户了
        String respXml=WxService.getRespose(requestMap);
        PrintWriter out=response.getWriter();
        out.print(respXml);
        out.flush();
        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        System.out.println("有人发出了get请求");
        PrintWriter out=response.getWriter();
        response.setContentType("text/html");
        out.println(" 这么隐秘的地方都能被你发现了，少年可以啊。这里是我的微信公众号开发后端专用端口。E-mail:326547630@qq.com");
        out.flush();
        out.close();
        //微信开发第一步，接入校验，会发送一条get请求，几乎全部的微信开发就只有这里是发送get请求的
        String signature=request.getParameter("signature");
        String timestamp=request.getParameter("timestamp");
        String nonce=request.getParameter("nonce");
        String echostr=request.getParameter("echostr");
        if(WxService.check(signature,timestamp,nonce,echostr)){
            System.out.println("校验通过");
            out=response.getWriter();
            out.print(echostr);
            out.flush();
            out.close();
            System.out.println("校验成功");
        }else{
            System.out.println("校验失败");
        }
    }
}
