package test;

import Service.WxService;
import entity.*;
import net.sf.json.JSONObject;
import org.jetbrains.annotations.TestOnly;
import org.junit.jupiter.api.Test;
import util.Main;

import java.util.Scanner;

import static util.Main.API_URL;

public class test {

    public static void main(String[] args){
      System.out.println(WxService.getAccessToken());
     // TestMenu();
        TestMubanMessage();
    }


    public static void TestMubanMessage(){
    String jsonStr=new String("");
    jsonStr=" {\n" +
            "          \"industry_id1\":\"2\",\n" +
            "          \"industry_id2\":\"25\"\n" +
            "       }";
    System.out.println(jsonStr);
    


    }

    @Test
    public static void TestMenu(){
        // 菜单对象
        Button btn = new Button();
//        // 第一个一级菜单
//        btn.getButton().add(new ClickButton("一级点击", "1"));
//        // 第二个一级菜单
//        btn.getButton().add(new ViewButton("一级跳转", "http://www.baidu.com"));
        // 创建第三个一级菜单
        SubButton sb1=new SubButton("关于我们");
        sb1.getSub_button().add(new ViewButton("团队新闻","https://mp.weixin.qq.com/s?__biz=MzIxNzA1NTU2Mw==&mid=2652123613&idx=1&sn=13c3bc3a60d5b0e1d9f694178eaef85a&chksm=8c1f39f9bb68b0ef49b6467210d7fb5528d20fe33bcc4a0fba3e9343b648cff2fb0d5be88ebc&mpshare=1&scene=1&srcid=1027Vqp69QJa9wlUm1KGzTRg"));
        sb1.getSub_button().add(new ViewButton("优秀员工","https://mp.weixin.qq.com/s?__biz=MzIxNzA1NTU2Mw==&mid=2652123948&idx=1&sn=798d15909cbacf06b0743b0e7bfc4866&chksm=8c1f3b08bb68b21e5a0ba9d3ea0adde50c0154d772bd0e13ef75a0a4780771771d02c33214bd&mpshare=1&scene=1&srcid=1213Www5VXZONtzPyQrbGB1j"));

        SubButton sb2 = new SubButton("实用工具");
        // 为第三个一级菜单增加子菜单
        sb2.getSub_button().add(new PhotoOrAlbumButton("传图", "31"));
        sb2.getSub_button().add(new ClickButton("模板消息", "32"));
        sb2.getSub_button().add(new ViewButton("集团官网", "http://www.wuling.com.cn/"));
        // 加入第三个一级菜单
        btn.getButton().add(sb1);
        btn.getButton().add(sb2);
        // 转为json
        JSONObject jsonObject = JSONObject.fromObject(btn);
        System.out.println(jsonObject.toString());

    }

}
